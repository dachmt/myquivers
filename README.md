MyQuivers.com
=============

This is the repository for the MyQuivers project.

Uber specific
-------------

##### This code has been proudly made by myself at 100%. The Javascript architecture was taken from a friend and ex-coworker Steve Lindstrom, currently working at Chairish, but still coded by myself.

I have been learning a lot during this project. It was the first time I was setting up an entire environment, and all the tools to deploy it and manage environments (dev, staging, prod).

First time as well using Heroku, and manage the configurations for an easy and good deployment on production. The Final result can be seen at http://www.myquivers.com

This project should not be online, as many things are not ready. Some more code was in the work but I put it in stand-by as it takes too much of my time, being alone from start to finish is not easy and require lots of hours. To add on top, most of the technologies used here were new to me, so the learning part is huge.

As far as the application and what it does, in this version it's really basic. Store and manage 3 quivers (set) of surfboards, to always have a trace of them somewhere, and remember them.

The goal was to:

* store and manage quivers of surfboards, but later on skis, snowboards, etc
* become a platform to find reviews
* be able to sell/trade/buy items, or even rent (and take a % of this price)
* use the data for ads, targeting shops so they can increase their advertising and sell more
* use the data to help surfers improve their surfing and help them in deciding what board is made for them
* and more ideas

I found another person with a similar project, more advanced and we are talking to see what I could help with, but the concept is the same.

The code itself uses a web application, no API yet, no front-end framework, pure Django/Python app.

Technologies:

* Vagrant and Chef solo
* Python/Django
* LESS/CSS
* HTML5
* Twitter Bootstrap, with basic free theme (using CSS not LESS)
* Jquery/Javascript
* Grunt (not used yet but for compiling LESS into CSS and minimifying JS and CSS)
* Heroku for production environment
* MySQL in development, Postgres in production (Heroku)
* AWS S3 for production static files

Installation
------------

1. Download [VirtualBox](https://www.virtualbox.org/)
2. Download [Vagrant](vagrantup.com)
3. Clone this repository:

    `git clone https://dachmt@bitbucket.org/dachmt/myquivers.git --recursive`

4. Go to wherever you cloned the repository and run the following command:

    `vagrant up`

5. Now you have a virtual machine with Python, MySQL, and a bunch of other stuff
6. In the same directory as the Vagrantfile type the following command to SSH into your VM:

    `vagrant ssh`

7. Type the following command to get your virtual env going:

    `activate` or `source /vagrant/platform/bin/activate`

8. Change to the /vagrant directory

    `cd /vagrant/`

9. Create DBs and apply migrations

    `python manage.py syncdb`

    `python manage.py myquivers`

    `python manage.py registration`

10. To run the website (in dev mode for now...) type the following command:

    `run_dev` or `python manage.py runserver 0.0.0.0:8000`

11. Navigate your browser to http://localhost:8001 to see the website. (Note: we forward port 8000 in the base machine to 8001 in the virtual machine)