#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    if os.environ.get('MYQUIVERS_PRODUCTION'):
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myquivers.settings.prod")
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myquivers.settings.dev")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)