from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from myquivers.models import User as CustomUser
from myquivers.models import Surfboard, SurfboardImage
from myquivers.models import Quiver


class SurfboardImageInline(admin.StackedInline):
    model = SurfboardImage
    extra = 1


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(
        help_text="Raw passwords are not stored, so there is no way to see "
                  "this user's password, but you can change the password "
                  "using <a href=\"password/\">this form</a>.")

    class Meta:
        model = CustomUser

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('email', 'username', 'first_name', 'last_name', 'gender', 'is_active', 'is_admin')
    list_filter = ('gender', 'is_active', 'is_admin')
    fieldsets = (
        (None, {
            'fields': ('email', 'username', 'password')
        }),
        ('Personal info', {
            'fields': ('first_name', 'last_name', 'gender', 'zip_code', 'height', 'weight', 'date_of_birth')
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_admin')
        }),
        ('General information', {
            'classes': ('collapse',),
            'fields': ('stance', 'year_started_surfing', 'home_spot', 'favorite_spots')
        }),
        ('Social information', {
            'classes': ('collapse',),
            'fields': ('fantasy_surfer_id',)
        }),
        ('Important dates', {
            'classes': ('collapse',),
            'fields': ('last_login', 'date_joined')
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1',
                       'password2', 'is_active', 'is_admin')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class SurfboardInline(admin.StackedInline):
    model = Surfboard
    extra = 0
    classes = ['collapse']


class SurfboardAdmin(admin.ModelAdmin):
    list_display = ('brand', 'model', 'length')
    list_filter = ('length', 'brand')
    inlines = [SurfboardImageInline,]
    search_fields = ('quiver__user__email',)
    ordering = ('brand',)


class QuiverAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'user', 'is_list')
    list_filter = ('type', 'name')
    fieldsets = (
        (None, {'fields': ('name', 'type', 'user', 'is_list',)}),
    )
    search_fields = ('user__email',)
    ordering = ('user',)
    inlines = [SurfboardInline,]


admin.site.unregister(Group)

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Quiver, QuiverAdmin)
admin.site.register(Surfboard, SurfboardAdmin)