from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q


class LoginBackend(ModelBackend):

    def authenticate(self, username=None, password=None):
        # Try to get the user from the email address or username.
        try:
            user = get_user_model().objects.get(Q(email=username) |
                                                Q(username=username))
        except get_user_model().DoesNotExist:
            return None

        if user.check_password(password):
            return user