# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'myquivers_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('date_of_birth', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=5, blank=True)),
            ('height', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('weight', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('stance', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('year_started_surfing', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('home_spot', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('favorite_spots', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fantasy_surfer_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'myquivers', ['User'])

        # Adding model 'Quiver'
        db.create_table(u'myquivers_quiver', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['myquivers.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=5)),
        ))
        db.send_create_signal(u'myquivers', ['Quiver'])

        # Adding model 'Surfboard'
        db.create_table(u'myquivers_surfboard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quiver', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['myquivers.Quiver'])),
            ('brand', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('length', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('width', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('thickness', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('volume', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'myquivers', ['Surfboard'])

        # Adding model 'SurfboardImage'
        db.create_table(u'myquivers_surfboardimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('surfboard', self.gf('django.db.models.fields.related.ForeignKey')(related_name='surfboard_images', to=orm['myquivers.Surfboard'])),
        ))
        db.send_create_signal(u'myquivers', ['SurfboardImage'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'myquivers_user')

        # Deleting model 'Quiver'
        db.delete_table(u'myquivers_quiver')

        # Deleting model 'Surfboard'
        db.delete_table(u'myquivers_surfboard')

        # Deleting model 'SurfboardImage'
        db.delete_table(u'myquivers_surfboardimage')


    models = {
        u'myquivers.quiver': {
            'Meta': {'object_name': 'Quiver'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['myquivers.User']"})
        },
        u'myquivers.surfboard': {
            'Meta': {'object_name': 'Surfboard'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'quiver': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['myquivers.Quiver']"}),
            'thickness': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'volume': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'width': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'myquivers.surfboardimage': {
            'Meta': {'object_name': 'SurfboardImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'surfboard': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'surfboard_images'", 'to': u"orm['myquivers.Surfboard']"})
        },
        u'myquivers.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'fantasy_surfer_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'favorite_spots': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'height': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'home_spot': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'stance': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'weight': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'year_started_surfing': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'})
        }
    }

    complete_apps = ['myquivers']