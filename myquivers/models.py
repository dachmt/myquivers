import datetime
import uuid
import os
import urllib
import hashlib

from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from django.utils import timezone


# Constants
GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

STANCE_CHOICES = (
    ('R', 'Regular'),
    ('G', 'Goofy'),
)

QUIVER_TYPES = (
    # ('BOD', 'Bodyboarding'),
    # ('CAN', 'Canoeing'),
    # ('FIS', 'Fishing'),
    # ('KIT', 'Kitesurfing'),
    # ('PAD', 'Paddleboarding'),
    # ('SKA', 'Skateboarding'),
    # ('SKI', 'Skiing'),
    # ('SKM', 'Skimboarding'),
    # ('SNO', 'Snowboarding'),
    ('SUR', 'Surfing'),
    # ('WAK', 'Wakeboarding'),
    # ('WIN', 'Windsurfing'),
)

SURFBOARD_TYPES = (
    ('SH', 'Shortboard'),
    ('HY', 'Hybrid'),
    ('FI', 'Fish'),
    ('EG', 'Egg'),
    ('GU', 'Gun'),
    ('LO', 'Longboard'),
    ('FU', 'Funboard'),
    ('SU', 'SUP'),
    ('OT', 'Other'),
)


# Functions
def get_images_upload_to(instance, filename):
    return instance.get_upload_to_path(filename)


def get_upload_path_for(type, filename):
    now = datetime.datetime.now()
    path = 'uploads/%s/%s' % \
           (type, now.strftime('%Y/%m/%d'))
    ext = filename.split('.')[-1]
    filename = '%s.%s' % (uuid.uuid4(), ext)
    return os.path.join(path, filename)


def validate_zip_code(value, length=5):
    if len(str(value)) != length:
        raise ValidationError(u'Should be a 5 digits number')


# Classes
class UserManager(BaseUserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        now = timezone.now()

        if not email:
            raise ValueError('Users must have an email address')

        email = UserManager.normalize_email(email)
        user = self.model(username=username, email=email,
                          is_active=True, is_admin=False,
                          last_login=now, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        u = self.create_user(username, email, password, **extra_fields)
        u.is_active = True
        u.is_admin = True
        u.save(using=self._db)
        return u


class User(AbstractBaseUser):
    username = models.CharField(max_length=255, unique=True, db_index=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)
    zip_code = models.CharField(
        verbose_name='ZIP code',
        max_length=5,
        validators=[validate_zip_code],
        blank=True
    )

    height = models.CharField(max_length=255, blank=True)
    weight = models.CharField(max_length=255, blank=True)

    stance = models.CharField(max_length=1, choices=STANCE_CHOICES, blank=True)
    year_started_surfing = models.CharField(max_length=255, blank=True)
    home_spot = models.CharField(max_length=255, blank=True)
    favorite_spots = models.TextField(blank=True)
    fantasy_surfer_id = models.IntegerField(null=True, blank=True)

    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def get_full_name(self):
      return u' '.join((self.first_name, self.last_name))

    def get_short_name(self):
      return self.first_name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def get_gravatar_url(self, size="50"):
        gravatar_url = "https://www.gravatar.com/avatar.php?"
        gravatar_url += urllib.urlencode({
            'gravatar_id': hashlib.md5(self.email).hexdigest(),
            'size': str(size),
        })

        return gravatar_url

    @property
    def is_staff(self):
        # All admins are staff
        return self.is_admin

    @property
    def is_superuser(self):
        # All admins are superusers
        return self.is_admin

    @property
    def gravatar_sm_url(self):
        return self.get_gravatar_url()

    @property
    def gravatar_lg_url(self):
        return self.get_gravatar_url(size="150")

    @property
    def gravatar_xl_url(self):
        return self.get_gravatar_url(size="300")


class Quiver(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=5, choices=QUIVER_TYPES)
    is_list = models.BooleanField(default=False)

    def last_4_surfboards(self):
        return self.surfboard_set.all().order_by('-id')[:4]

    def __unicode__(self):
        return u'%s - %s quiver (%s)' % (
            self.user.username,
            self.name,
            self.get_type_display())


class Surfboard(models.Model):
    quiver = models.ForeignKey(Quiver)
    type = models.CharField(max_length=2, choices=SURFBOARD_TYPES, default='SH')
    brand = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    length = models.CharField(max_length=255)
    width = models.CharField(max_length=255)
    thickness = models.CharField(max_length=255)
    volume = models.CharField(max_length=255, blank=True)
    note = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.length, self.brand, self.model)

    def get_size(self):
        "Returns the size for the surfboards"
        return u'%s x %s x %s' % (self.length, self.width, self.thickness)

    def get_cover_image(self):
        "Returns the last uploaded image"
        return self.surfboard_images.latest('id')


class Image(models.Model):
    image = models.ImageField(upload_to=get_images_upload_to)

    class Meta:
        abstract = True

    def get_upload_to_path(instance, filename):
        return get_upload_path_for('images', filename)


class SurfboardImage(Image):
    surfboard = models.ForeignKey(Surfboard, related_name='surfboard_images')

    def get_upload_to_path(instance, filename):
        return get_upload_path_for('images/surfboards', filename)


# Signals
@receiver(pre_delete, sender=SurfboardImage)
def image_delete(sender, instance, **kwargs):
    if instance.image is not None:
        instance.image.delete(False)

@receiver(post_save, sender=User)
def create_default_quivers(sender, created, instance, **kwargs):
    if created:
        Quiver.objects.create(user=instance, name='Past surfboards', type='SUR')
        Quiver.objects.create(user=instance, name='Current quiver', type='SUR')
        Quiver.objects.create(user=instance, name='Wish list', type='SUR', is_list=True)