from myquivers.settings.common import *


DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Database: MySQL
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'quiverator',                 # Or path to database file if using sqlite3.
        'USER': 'dba',
        'PASSWORD': 'test654',
        'HOST': '',                           # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                           # Set to empty string for default.
    }
}

INSTALLED_APPS = INSTALLED_APPS + (
    'debug_toolbar',
)

MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INTERNAL_IPS = (
    '127.0.0.1',
    '10.0.2.2',
)

# django-toolbar
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

# Allows to see email sent in the console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
