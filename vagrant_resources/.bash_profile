# shortcuts
alias c='clear;'
alias ll='ls -alv;'
alias activate='source /vagrant/bin/activate'
alias run_dev='python /vagrant/manage.py runserver 0.0.0.0:8000'

# terminal colors
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
