CREATE USER 'dba'@'localhost' IDENTIFIED BY 'test654';
GRANT USAGE ON *.* TO 'dba'@'localhost';
GRANT ALL PRIVILEGES ON quiverator.* TO 'dba'@'localhost';
FLUSH PRIVILEGES;