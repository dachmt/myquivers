#!/usr/bin/env bash

eval vagrantfile_location="~/.vagrantfile_processed"

if [ -f $vagrantfile_location ]; then
  echo "Vagrantfile already processed. Exiting..."
  exit 0
fi

#==================================================================
# install dependencies
#==================================================================

/usr/bin/yes | pip install --upgrade pip
/usr/bin/yes | pip install --upgrade virtualenv

/usr/bin/yes | sudo apt-get install python-software-properties
/usr/bin/yes | sudo apt-get install build-essential python-dev libmysqlclient-dev libpq-dev

#==================================================================
# set up the local dev environment
#==================================================================

if [ -f "/home/vagrant/.bash_profile" ]; then
    echo -n "removing .bash_profile for user vagrant..."
    rm /home/vagrant/.bash_profile
    echo "done!"
fi

echo -n "creating new .bash_profile for user vagrant and links..."
ln -s /vagrant/vagrant_resources/.bash_profile /home/vagrant/.bash_profile
chmod +x /vagrant/vagrant_resources/pre-commit
ln -s /vagrant/vagrant_resources/pre-commit /vagrant/.git/hooks/pre-commit
chmod +x /vagrant/.git/hooks/pre-commit
source /home/vagrant/.bash_profile
echo "done!"

#==================================================================
# set up virtual env
#==================================================================
cd /vagrant;
echo -n "Creating virtualenv..."
virtualenv .;
echo "done!"

echo -n "Activating virtualenv..."
source /vagrant/bin/activate
echo "done!"

echo -n "Setting up env variables..."
/usr/bin/yes | export DJANGO_DEBUG=True
echo "done!"

echo -n "installing project dependencies via pip..."
/usr/bin/yes | pip install -r /vagrant/requirements/dev.txt
/usr/bin/yes | pip install MySQL-python
echo "done!"

#==================================================================
# create default databases
#==================================================================

# create the database and users

echo -n "creating default mysql databases..."
mysql -u root -ptest654 < /vagrant/vagrant_resources/sql/create_databases.sql;
echo "done!"

echo -n "creating default mysql roles..."
mysql -u root -ptest654 < /vagrant/vagrant_resources/sql/create_users.sql;
echo "done!"

#==================================================================
# install front-endy things
#==================================================================

echo -n "adding node.js npm repo..."
add-apt-repository ppa:chris-lea/node.js &> /dev/null || exit 1
echo "done!"

echo -n "calling apt-get update..."
apt-get update &> /dev/null || exit 1
echo "done!"

echo -n "nodejs and npm..."
apt-get install nodejs npm &> /dev/null || exit 1
echo "done!"

echo -n "installing grunt..."
npm install -g grunt-cli &> /dev/null || exit 1
echo "done!"

echo -n "installing LESS..."
npm install -g less &> /dev/null || exit 1
echo "done!"

echo -n "installing uglify.js..."
npm install -g uglify-js &> /dev/null || exit 1
echo "done!"

#==================================================================
# cleanup
#==================================================================

echo -n "marking vagrant as processed..."
touch $vagrantfile_location
echo "done!"