from django import forms
from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm, \
    PasswordResetForm, SetPasswordForm, PasswordChangeForm
from django.forms import ModelForm, ChoiceField

from myquivers.models import User, GENDER_CHOICES, STANCE_CHOICES
from registration.forms import RegistrationFormUniqueEmail


# To add empty value to ChoiceField object...
GENDER_CHOICES = (('', 'Select your gender'),) + GENDER_CHOICES
STANCE_CHOICES = (('', 'Select your stance'),) + STANCE_CHOICES


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label="Email",
        max_length=255,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Email or username'}
        )
    )
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Password'}
        )
    )
    remember_me = forms.BooleanField(required=False, initial=True)

    def login(self, request):
        if self.is_valid():
            login(request, self.user)
            if self.cleaned_data['remember_me']:
                request.session.set_expiry(60 * 60 * 24 * 7 * 4) # 4 weeks
            else:
                request.session.set_expiry(0) # browser closes
            return True
        return False


class CustomRegistrationForm(RegistrationFormUniqueEmail):
    username = forms.RegexField(
        regex=r'^[\w.@+-]+$',
        max_length=30,
        min_length=3,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Username'}
        ),
        error_messages=
        {'invalid': "This value must contain only letters, numbers and underscores."}
    )
    email = forms.EmailField(
        max_length=75,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Email'}
        ),
    )
    password1 = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Password'},
            render_value=False
        ),
        label="Password"
    )

    def __init__(self, *args, **kwargs):
        super(CustomRegistrationForm,self).__init__(*args, **kwargs)
        del self.fields['password2']

    def clean(self):
        "Removes passwords verification."
        return self.cleaned_data


class CustomPasswordResetForm(PasswordResetForm):
    email = forms.EmailField(
        max_length=75,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Email'}
        ),
        label="Email"
    )


class CustomSetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'New password'},
            render_value=False
        ),
        label="New password"
    )
    new_password2 = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'New password confirmation'},
            render_value=False
        ),
        label="New password confirmation"
    )


class CustomPasswordChangeForm(PasswordChangeForm):
    old_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Old password'},
            render_value=False
        ),
        label="Old password"
    )
    new_password1 = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'New password'},
            render_value=False
        ),
        label="New password"
    )
    new_password2 = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': 'New password confirmation'},
            render_value=False,

        ),
        label="New password confirmation"
    )


class ProfileForm(ModelForm):
    gender = ChoiceField(choices=GENDER_CHOICES, required=False)
    stance = ChoiceField(choices=STANCE_CHOICES, required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'date_of_birth', 'gender',
                  'zip_code', 'height', 'weight', 'stance',
                  'year_started_surfing', 'home_spot', 'favorite_spots',
                  'fantasy_surfer_id']

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if isinstance(field.widget, forms.TextInput) or \
                isinstance(field.widget, forms.Textarea) or \
                isinstance(field.widget, forms.DateInput) or \
                isinstance(field.widget, forms.DateTimeInput) or \
                isinstance(field.widget, forms.Select) or \
                isinstance(field.widget, forms.TimeInput):
                field.widget.attrs.update({'placeholder': field.label,
                                           'class': 'form-control'})