from django.conf.urls import patterns, url, include
from django.contrib.auth.views import password_reset, \
    password_change_done, password_reset_done, \
    password_reset_confirm, password_reset_complete, password_change

from webapp.account.forms import LoginForm, \
    CustomRegistrationForm, CustomPasswordResetForm, \
    CustomPasswordChangeForm, CustomSetPasswordForm
from webapp.account.views import CustomRegistrationView


urlpatterns = patterns('',
    url(r'^login/$',
        'django.contrib.auth.views.login',
        {'authentication_form': LoginForm},
        name='login'),
    url(r'^logout/$',
        'django.contrib.auth.views.logout',
        {'next_page': '/'},
        name='logout'),
    url(r'^password/change/$', password_change,
        {'password_change_form' : CustomPasswordChangeForm},
        name='password_change'),
    url(r'^password/change/done/$', password_change_done),
    url(r'^password/reset/$', password_reset,
        {'password_reset_form' : CustomPasswordResetForm,
         'post_reset_redirect' : '/account/password/reset/done/'},
        name='password_reset'),
    url(r'^password/reset/done/$', password_reset_done),
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm,
        {'set_password_form' : CustomSetPasswordForm,
         'post_reset_redirect' : '/account/password/done/'},
        name='password_confirm'),
    url(r'^password/done/$', password_reset_complete),
    url(r'^profile/$', 'webapp.account.views.profile', name='edit_profile'),
    url(r'^profile/picture/$', 'webapp.account.views.profile_picture', name='profile_picture'),
    url(r'^register/$',
        CustomRegistrationView.as_view(
            form_class=CustomRegistrationForm,
            success_url='/'),
        name='register'),

    url(r'', include('registration.backends.simple.urls')),
)