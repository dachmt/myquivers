from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

from registration.backends.simple.views import RegistrationView
from webapp.account.forms import ProfileForm


class CustomRegistrationView(RegistrationView):
    def get_success_url(self, request, user):
        # TODO: send welcome email when registration done
        # user.email_user(subject, message, from_email)
        return (settings.LOGIN_REDIRECT_URL, (), {})


def custom_proc(request):
    "A context processor that provides general data."
    return {
        'module': 'account',
    }


@login_required
def profile(request):
    user = request.user

    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, instance=user)
        if profile_form.is_valid():
            profile_form.save()
    else:
        profile_form = ProfileForm(instance=user)

    context = {
        'action': 'profile',
        'form': profile_form
    }

    return render_to_response('account/profile.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def profile_picture(request):
    context = {
        'action': 'profile_picture',
    }

    return render_to_response('account/profile_picture.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))