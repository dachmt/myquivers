from django.contrib.sites.models import Site
from myquivers.models import Quiver


def site(request):
    try:
        site = Site.objects.get_current()
        return {'site': site}
    except Site.DoesNotExist:
        return {'site': ''}

def navbar(request):
    if request.user.is_authenticated():
        user_quivers = Quiver.objects.filter(
            user=request.user,
            type='SUR'
        )

        quiver_id_to_quiver_name_map = {}
        for quiver in user_quivers:
            quiver_id_to_quiver_name_map[quiver.id] = quiver.name

        return {
            'quiver_id_to_quiver_name_map': quiver_id_to_quiver_name_map
        }
    else:
        return {}

def default(request):
    return {
        'ip_address': request.META['REMOTE_ADDR'],
        'gravatar_url': 'https://www.gravatar.com',
        'facebook_url': 'https://www.facebook.com/myquivers',
        'twitter_url': 'https://twitter.com/dachmt',
        'instagram_url': 'http://instagram.com/dachmt',
    }
