from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

from myquivers.models import Quiver


def custom_proc(request):
    "A context processor that provides general data."
    return {
        'module': 'home',
    }


@login_required
def index(request):
    quivers = Quiver.objects.filter(
        user=request.user,
        type='SUR'
    )

    context = {
        'action': 'index',
        'quivers': quivers,
    }

    return render_to_response('home/index.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))
