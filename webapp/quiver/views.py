from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from myquivers.models import Quiver, Surfboard


def custom_proc(request):
    "A context processor that provides general data."
    return {
        'module': 'quiver',
    }

@login_required
def show(request, quiver_id):
    quiver = get_object_or_404(Quiver, pk=quiver_id)

    if request.user.id != quiver.user.id:
        raise PermissionDenied()

    surfboards = Surfboard.objects.filter(
        quiver__id=quiver_id,
        quiver__user=request.user,
        quiver__type='SUR',
    ).order_by('-id')

    context = {
        'action': 'show',
        'quiver': quiver,
        'surfboards': surfboards
    }

    return render_to_response('quiver/show.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))
