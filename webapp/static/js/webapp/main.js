/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {

    'use strict';

    var MYQUIVERS = window.myquivers = {};

    $(document).ready(function () {

        $('.swipebox').swipebox();
        $('.social-links a').tooltip();

        $('.datepicker input').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "-100:+0" // last hundred years
        });

        $('.surfboard-delete').click(function(e) {
            e.preventDefault();
            if(confirm("Are you sure you want to delete this surfboard?")) {
                document.location = $(this).attr('href');
            }
         });

        var $body = $('body'),
            module = $body.data('module-name'),
            action = $body.data('action-name'),
            pages = MYQUIVERS.pages || {},

            /**
             * Determines if a module/action has code that needs to be run
             *
             * @param string module
             * @param string action
             * @return boolean
             */
            executeIfPossible = function (module, action) {
                if (pages[module]  && pages[module][action] && $.isFunction(pages[module][action])) {
                    pages[module][action]();
                }
            };

        executeIfPossible('global', 'common'); // code that touches every page
        executeIfPossible(module, 'common');   // common code in a given module
        executeIfPossible(module, action);     // page specific code
    });

}(jQuery, window, document));