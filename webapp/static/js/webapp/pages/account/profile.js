/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        account = pages.account = pages.account || {};

    /**
     * Page specific javascript for account/profile
     */
    account.profile = function () {
        console.log('account profile information page');
    };

}(jQuery, window, document));