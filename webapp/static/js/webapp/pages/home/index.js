/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        home = pages.home = pages.home || {};

    /**
     * Page specific javascript for home/index
     */
    home.index = function () {
        console.log('signed-in homepage');
    };

}(jQuery, window, document));