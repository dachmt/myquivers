/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        quiver = pages.quiver = pages.quiver || {};

    /**
     * Page specific javascript for quiver/show
     */
    quiver.show = function () {
        console.log('quiver show page');
    };

}(jQuery, window, document));