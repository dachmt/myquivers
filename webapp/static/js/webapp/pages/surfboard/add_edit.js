/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        surfboard = pages.surfboard = pages.surfboard || {};

    /**
     * Page specific javascript for surfboard/add
     */
    surfboard.add = function () {
        console.log('surfboard add page');
    };

    /**
     * Page specific javascript for surfboard/add
     */
    surfboard.edit = function () {
        console.log('surfboard edit page');
    };

}(jQuery, window, document));