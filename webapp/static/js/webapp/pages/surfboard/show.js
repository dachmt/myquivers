/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        surfboard = pages.surfboard = pages.surfboard || {};

    /**
     * Page specific javascript for surfboard/show
     */
    surfboard.show = function () {
        console.log('surfboard show page');
    };

}(jQuery, window, document));