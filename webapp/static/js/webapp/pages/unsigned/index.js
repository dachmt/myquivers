/*global jQuery, window, document, undefined*/
;(function ($, window, document, undefined) {
    'use strict';

    var $document = $(document),

        MYQUIVERS = window.myquivers || {},
        pages = MYQUIVERS.pages = MYQUIVERS.pages || {},
        unsigned = pages.unsigned = pages.unsigned || {};

    /**
     * Page specific javascript for unsigned/index
     */
    unsigned.index = function () {
        console.log('signed-out homepage');
    };

}(jQuery, window, document));