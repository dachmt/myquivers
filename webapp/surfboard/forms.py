from django import forms
from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from myquivers.models import Surfboard, SurfboardImage


class SurfboardForm(ModelForm):
    class Meta:
        model = Surfboard

    def __init__(self, *args, **kwargs):
        super(SurfboardForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if isinstance(field.widget, forms.TextInput) or \
                isinstance(field.widget, forms.Textarea) or \
                isinstance(field.widget, forms.DateInput) or \
                isinstance(field.widget, forms.DateTimeInput) or \
                isinstance(field.widget, forms.Select) or \
                isinstance(field.widget, forms.TimeInput):
                field.widget.attrs.update({'placeholder': field.label,
                                           'class': 'form-control input-xlarge'})

SurfboardImageFormset = inlineformset_factory(Surfboard,
                                              SurfboardImage,
                                              fields=('image',),
                                              can_delete=True,
                                              extra=4,
                                              max_num=4)