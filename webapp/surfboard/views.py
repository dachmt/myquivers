from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from myquivers.models import Quiver, Surfboard
from webapp.surfboard.forms import SurfboardForm, SurfboardImageFormset


def custom_proc(request):
    "A context processor that provides general data."
    return {
        'module': 'surfboard',
    }

@login_required
def show(request, surfboard_id):
    surfboard = get_object_or_404(Surfboard, pk=surfboard_id)

    if request.user.id != surfboard.quiver.user.id:
        raise PermissionDenied()

    has_multiple_images = False
    if surfboard.surfboard_images.count() > 1:
        has_multiple_images = True

    context = {
        'action': 'show',
        'surfboard': surfboard,
        'has_multiple_images': has_multiple_images,
    }

    return render_to_response('surfboard/show.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def add(request):
    user = request.user

    image_formset = SurfboardImageFormset(instance=Surfboard())

    if request.method == 'POST':
        surfboard_form = SurfboardForm(request.POST)
        if surfboard_form.is_valid():
            surfboard = surfboard_form.save()
            image_formset = SurfboardImageFormset(
                request.POST,
                request.FILES,
                instance=surfboard
            )
            if image_formset.is_valid():
                image_formset.save()

            return HttpResponseRedirect(reverse('surfboard_show', args=(surfboard.id,)))
    else:
        surfboard_form = SurfboardForm()

    surfboard_form.fields['quiver'].queryset = user.quiver_set.all()
    surfboard_form.fields['quiver'].empty_label = None
    surfboard_form.fields['quiver'].label_from_instance = lambda obj: '%s' % (obj.name)

    context = {
        'action': 'add',
        'form': surfboard_form,
        'image_formset': image_formset
    }

    return render_to_response('surfboard/form.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def edit(request, surfboard_id):
    surfboard = get_object_or_404(Surfboard, pk=surfboard_id)
    user = request.user

    if user.id != surfboard.quiver.user.id:
        raise PermissionDenied()

    surfboard_form = SurfboardForm(instance=surfboard)
    image_formset = SurfboardImageFormset(instance=surfboard)

    surfboard_form.fields['quiver'].queryset = user.quiver_set.all()
    surfboard_form.fields['quiver'].empty_label = None
    surfboard_form.fields['quiver'].label_from_instance = lambda obj: '%s' % (obj.name)

    if request.method == 'POST':
        surfboard_form = SurfboardForm(request.POST, instance=surfboard)
        if surfboard_form.is_valid():
            surfboard = surfboard_form.save(commit=False)
            image_formset = SurfboardImageFormset(
                request.POST,
                request.FILES,
                instance=surfboard
            )
            if image_formset.is_valid():
                surfboard.save()
                image_formset.save()

            return HttpResponseRedirect(reverse('surfboard_show', args=(surfboard.id,)))

    context = {
        'action': 'edit',
        'form': surfboard_form,
        'image_formset': image_formset,
        'surfboard': surfboard
    }

    return render_to_response('surfboard/form.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def delete(request, surfboard_id):
    surfboard = get_object_or_404(Surfboard, pk=surfboard_id)

    if request.user.id != surfboard.quiver.user.id:
        raise PermissionDenied()

    quiver_id = surfboard.quiver_id
    surfboard.delete()

    return HttpResponseRedirect(reverse('quiver_show', args=[quiver_id]))