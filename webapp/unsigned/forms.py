from django import forms

class ContactForm(forms.Form):
    subject = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Tell us why you are contacting us'}
        )
    )
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'class': 'form-control',
                   'placeholder': 'Message'}
        )
    )
    sender = forms.EmailField(
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Please enter your email address'}
        ),
        label='Email'
    )