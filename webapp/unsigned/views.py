from django import forms
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, BadHeaderError
from django.shortcuts import render_to_response
from django.template import RequestContext

from webapp.account.forms import CustomRegistrationForm
from webapp.unsigned.forms import ContactForm


def custom_proc(request):
    "A context processor that provides general data."
    return {
        'module': 'unsigned',
    }

def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    context = {
        'action': 'index',
        'registration_form': CustomRegistrationForm()
    }

    return render_to_response('unsigned/index.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

def about(request):
    context = {
        'action': 'about',
    }

    return render_to_response('unsigned/about.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

def terms(request):
    context = {
        'action': 'terms',
    }

    return render_to_response('unsigned/terms.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

def privacy(request):
    context = {
        'action': 'privacy',
    }

    return render_to_response('unsigned/privacy.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))

def contact(request):
    current_site = Site.objects.get_current()
    confirmation_message = None

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message'] + \
                      '\r\n\r\nSent via contact form at ' + \
                      current_site.domain + reverse('contact')

            sender = form.cleaned_data['sender']

            try:
                send_mail(subject, message, sender, [settings.DEFAULT_FROM_EMAIL])
                form = ContactForm()
                confirmation_message = 'Your message has been sent. ' \
                                       'We\'ll get back to you as soon as possible.'
            except BadHeaderError:
                errors = form._errors.setdefault(
                    forms.forms.NON_FIELD_ERRORS, forms.util.ErrorList()
                )
                errors.append(
                    'There was an error while trying to '
                    'send the email. Please try again.'
                )
    else:
        form = ContactForm()

    context = {
        'action': 'contact',
        'form': form,
        'confirmation_message': confirmation_message
    }

    return render_to_response('unsigned/contact.html', context,
                              context_instance=RequestContext(request, processors=[custom_proc]))
