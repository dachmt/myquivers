from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'webapp.unsigned.views.index', name='index'),
    url(r'^about-us/$', 'webapp.unsigned.views.about', name='about'),
    url(r'^account/', include('webapp.account.urls')),
    url(r'^contact-us/$', 'webapp.unsigned.views.contact', name='contact'),
    url(r'^home/$', 'webapp.home.views.index', name='home'),
    url(r'^privacy-policy/$', 'webapp.unsigned.views.privacy', name='privacy'),
    url(r'^quiver/(?P<quiver_id>\d+)/$', 'webapp.quiver.views.show', name='quiver_show'),
    url(r'^surfboard/(?P<surfboard_id>\d+)/$', 'webapp.surfboard.views.show', name='surfboard_show'),
    url(r'^surfboard/add/$', 'webapp.surfboard.views.add', name='surfboard_add'),
    url(r'^surfboard/delete/(?P<surfboard_id>\d+)/$', 'webapp.surfboard.views.delete', name='surfboard_delete'),
    url(r'^surfboard/edit/(?P<surfboard_id>\d+)/$', 'webapp.surfboard.views.edit', name='surfboard_edit'),
    url(r'^terms-of-service/$', 'webapp.unsigned.views.terms', name='terms'),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
